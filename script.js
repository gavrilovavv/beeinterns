$(function() {
    let btnSwitch = document.getElementById('btn-switch');

    btnSwitch.addEventListener('click', function () {
        btnSwitch.classList.toggle('checked');
    });

    let submenuItem = document.getElementById('submenu');
    let lecturesItem = document.getElementById('lectures');
    let linkSubmenu = document.getElementById('link-submenu');

    lecturesItem.addEventListener('click', function () {
        submenuItem.classList.toggle('active');
        linkSubmenu.classList.toggle('link-submneu_open');
    });

    let carousels  = document.getElementsByClassName('owl-carousel');
    for (let i = 0; i < carousels.length; i++) {
        let carousel = carousels[i];
        let owl = $(carousel);

        owl.owlCarousel({
            items: 6,
            autoWidth: 255,
            loop: false,
            nav: false,
            dots: false
        });

        let prevBtn = document.querySelector(`.owl-prev[data-carousel-id=${carousel.id}]`);
        prevBtn.addEventListener('click', function () {
            owl.trigger('prev.owl.carousel');
        });

        let nextBtn = document.querySelector(`.owl-next[data-carousel-id=${carousel.id}]`);
        nextBtn.addEventListener('click', function () {
            owl.trigger('next.owl.carousel');
        });
    }

    let dataGroup = document.querySelectorAll('#all-lectures-carousel [data-group]');
    let btnsFilter = document.querySelectorAll('.carousel-control__button_filter');

    function hideUnfiltered (param) {
        return param.style.display = 'none';
    }

    for (let i = 0; i < btnsFilter.length; i++) {
        btnsFilter[i].addEventListener('click', function () {
            let activeFilter = document.querySelectorAll('.active-filter');
            let currentActiveFilter = activeFilter[0];

            currentActiveFilter.classList.remove('active-filter');
            this.classList.add('active-filter');

            for (let j = 0; j < dataGroup.length; j++) {
                if (btnsFilter[i].dataset.group === 'allLectures') {
                    dataGroup[j].style.display = 'block';
                } else if (btnsFilter[i].dataset.group !== dataGroup[j].dataset.group) {
                    hideUnfiltered(dataGroup[j]);
                } else {
                    dataGroup[j].style.display = 'block';
                }
            }
        });
    }
});
