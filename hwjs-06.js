(() => {
    const MAX_LENGTH = 20;
    
    let slideTitle = document.querySelectorAll('#beginner-carousel .slide-info__title');
    let slideParagraph = document.querySelectorAll('#beginner-carousel .slide-info__paragraph');
    
    for (let i = 0; i < slideTitle.length; i++) {
        slideTitle[i].textContent = slideTitle[i].textContent.toUpperCase();
    }

    for (let i = 0; i < slideParagraph.length; i++) {
        if (slideParagraph[i].textContent.length > MAX_LENGTH) {
            slideParagraph[i].textContent = slideParagraph[i].textContent.slice(0, MAX_LENGTH) + '...';
        }
    }
})();
