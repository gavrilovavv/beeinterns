function allLectures() {
    let slide = document.querySelectorAll('.slide');
    let allLecturesCarousel = document.querySelector('#all-lectures-carousel');

    for (let i = 0; i < slide.length; i++) {
        allLecturesCarousel.append(slide[i].cloneNode(true));
    }

    let countLectures = document.querySelectorAll('#all-lectures-carousel .slide');
    let numberOfLectures = document.querySelector('#allLectures');

    numberOfLectures.innerHTML = countLectures.length + ' лекций';
}