function getObject() {    
    let slides = document.querySelectorAll('#all-lectures-carousel .slide');
    let objectLectures = {};

    slides.forEach(slide => {
        let group = slide.dataset.group;
       
        if (!objectLectures[group]) {
            objectLectures[group] = [];
        }

        objectLectures[group].push({
            title: slide.querySelector('.slide-info__title').textContent,
            description: slide.querySelector('.slide-info__paragraph').textContent,
            date: slide.querySelector('.slide-additional__date').textContent,
            image: slide.querySelector('.slide__img').getAttribute('src'),
            label: slide.querySelector('.slide__label_lecture').textContent,
        }); 
    })

    return objectLectures;
};

window.addEventListener('load', function() {
    console.log(getObject());
});
