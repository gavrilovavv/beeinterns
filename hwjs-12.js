// Выполнить задания и в комментариях пояснить, почему получили такой результат:
// 1)
const greetPerson = () => {
  const sayName = (name) => {
    return `hi, i am ${name}`
  }
  return sayName
}

const greeting = greetPerson(); // В константу greeting записываем функцию greetPerson
console.log(greeting('Pavel')); // В консоль выводится hi, i am Pavel, т.к. в функцию sayName передается параметр Pavel,
// в функции greetPerson возвращаем значение функции sayName, в которой в свою очередь возвращается строка с приветствием
console.log(greeting('Irina')); // аналогично, в консоль будет выведено hi, i am Irina

// 2)
let y = 'test';
const foo = () => {
  // var newItem = 'hello';
  console.log(y);
}
foo(); // при вызове этой функции в консоль будет выведено значение переменной y (test), т.к. эта переменная глобальная
console.log(newItem); // будет выведено сообщение об ошибке, т.к. строчка закомментирована, но если раскоментировать эту стрчоку,
// то рузельтат будет такой же, так как переменная newItem является локальной

// 3)
let y = 'test';
let test = 2;
const foo = () => {
  const test = 5;
  const bar = () => {
    console.log(5 + test);
  }
  bar()
}
foo();
// В консоль будет выведено 10, т.к. значение переменной test будет взято из ближайшего объявления, т.е. из foo

// 4)
const bar = () => {
  const b = 'no test'
}
bar();

const foo = (() => {
  console.log(b);
})();
const b = 'test';
// В функции foo отстутствует доступ к переменной b, поэтому будет выведена ошибка, так как
// у нее нет доступа к b из функции bar и объявление b в глобальной области видимости произошло после вызова функции foo
